cmake_minimum_required (VERSION 2.6)
project ("Silent Bob")
include_directories ("include", "gclib/include")
link_directories ("./")
set (LINK_FLAGS, "-L./ -lsblib")
add_library (sblib SHARED gclib/src/darray.cxx
			gclib/src/deprecated_dsplit.cxx
			gclib/src/dhash.cxx
			gclib/src/dheapsort.cxx
			gclib/src/djobs.cxx
			gclib/src/dlist.cxx
			gclib/src/dsplit.cxx
			gclib/src/earray.cxx
			gclib/src/ehash.cxx
			gclib/src/elist.cxx
			gclib/src/fs.cxx
			gclib/src/gclib.cxx
			gclib/src/gclib_c.cxx
			sblib/py_tt.cxx
			sblib/Sblib.cxx
			sblib/the_fly.cxx
			sblib/the_tt.cxx
			sblib/t_op2.cxx
			sblib/t_op.cxx
			sblib/TT.cxx
			sblib/wit.cxx )

link_libraries (sblib)
add_executable (silent_bob src/callTags.cxx
			src/cFiles.cxx
			src/cgrep.cxx
			src/ctags.cxx
			src/file.cxx
			src/indent.cxx
			src/init.cxx
			src/interactive.cxx
			src/kinds.cxx
			src/main.cxx
			src/modding.cxx
			src/opts_funcs.cxx
			src/opts_kinds.cxx
			src/opts_list.cxx
			src/opts_settings.cxx
			src/structs.cxx
			src/tree.cxx
			src/usage.cxx )

add_dependencies (silent_bob sblib)

#add_library (plugin_cache SHARED plugins/plugin_cache.cxx)
add_library (plugin_editor SHARED plugins/plugin_editor.cxx)
add_library (plugin_grep SHARED plugins/plugin_grep.cxx)
add_library (plugin_perl SHARED plugins/plugin_perl.cxx)
add_library (plugin_perlpackages SHARED plugins/plugin_perlpackages.cxx)
add_library (plugin_python SHARED plugins/plugin_python.cxx)
add_library (plugin_ruby SHARED plugins/plugin_ruby.cxx)
add_library (plugin_ruby_newclass SHARED plugins/plugin_ruby_newclass.cxx)

install (TARGETS sblib silent_bob RUNTIME DESTINATION bin 
				  LIBRARY DESTINATION lib )
