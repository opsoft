/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>

extern "C" DArray * plugin_init (struct env_t *env);
int ruby_ctags (char * f_name, FILE * of);
int ruby_kinds_file (char * f_name);
int ruby_kinds ();
FILE * ofile;
int rkinds = 0;

namespace Ruby
{
	enum {
		Class = (1<<0),
		Module = (1<<1),
		Function = (1<<2)
	};
};

void ruby_set_rkinds (char *S)
{
	while (*S) {
		switch (*S) {
			case 'c':
				rkinds |= Ruby::Class;
				break;
			case 'm':
				rkinds |= Ruby::Module;
				break;
			case 'f':
				rkinds |= Ruby::Function;
				break;
		}
		++S;
	}
}

char ruby_kinds_opt (DArray * d_opts, int * pos)
{
	if (! d_opts || ! pos)
		return 0;

	if (EQ (d_opts->get (*pos), "--ruby")) {
		ENV->language = (char *) "Ruby";
		return 1;
	}

	if (NE (ENV->language, "Ruby"))
		return 0;

	if (EQ (d_opts->get (*pos), "--kinds")) {
		if (++(*pos) >= d_opts->get_size ()) 
			return 0; 
		return 1;
	}
	
	return 0;
}

char ruby_kinds_opt2 (DArray * d_opts, int * pos)
{
	if (EQ (d_opts->get (*pos), "--kinds")) {
		if (++(*pos) >= d_opts->get_size ()) 
			return 0; 
		ruby_set_rkinds (d_opts->get(*pos));
		ruby_kinds ();
		exit (0);
	}

	return 0;
}

int ruby_kinds_scan_line (const char *S)
{
	char * ptr;
	if (! S)
		return 0;

	ptr = strchr  (S, '#');
	if (ptr)
		*ptr = '\0';

	if (strstr (S, "class "))
		return Ruby::Class;
	if (strstr (S, "module "))
		return Ruby::Module;
	if (strstr (S, "def "))
		return Ruby::Function;

	return 0;
}

char * ruby_kinds_tag (char * str)
{
	char * S;
	S = ruby_last_word (str);
	if (! S)
		return NULL;
	chomp (S);
	return S;
}

int ruby_kinds ()
{
	int i;
	DArray * files;

	files = ENV->d_files;
	for (i = 0; i < files->get_size (); ++i) 
		ruby_kinds_file (files->get (i));

	return 0;
}

int ruby_kinds_file (char * f_name)
{
	char * buf;
	int line;
	FILE * myfile;
	char * S;
	int kind;

	myfile = fopen (f_name, "r");
	if (! myfile)
		return -1;

	line = 0;
	buf = CNEW (char, 4096);
	while (fgets (buf, 4096, myfile)) {
		++line;
		kind =  ruby_kinds_scan_line (buf);
		if (! kind)
			continue;
		
		S = ruby_kinds_tag (buf);
		if (kind & rkinds) 
			printf ("%s\n", S);
	}

	fclose (myfile);
	DROP (buf);
	return 0;
}

void ruby_kinds_short_info ()
{
	printf ("Ruby kinds plugin.");
}

void ruby_kinds_long_info ()
{
	printf ("Ruby kinds plugin.\n");
	printf ("Version: 1.0\n");
	printf ("options: --kinds\n");
	printf ("Usage:  bob_ruby <files> --kinds <kinds>\n"
			"\t\t kinds: c - class, f - function, m - module\n ");
}	

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	mod_t * plug;

	Ret = new DArray (1);
	plug = CNEW (mod_t, 1);
	memset (plug, 0, sizeof (mod_t));

	plug->Version = strdup ("1.0");
	plug->short_info = ruby_kinds_short_info;
	plug->long_info = ruby_kinds_long_info;
	plug->opt = ruby_kinds_opt;
	plug->opt2 = ruby_kinds_opt2;

	ENV->listOptions->add (	"--ruby");

	Ret->add (LPCHAR (plug));
	return Ret;
}

