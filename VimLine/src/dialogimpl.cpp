#include "dialogimpl.h"
//
DialogImpl::DialogImpl( QWidget * parent, Qt::WFlags f) 
	: QDialog(parent, f)
{
	setupUi(this);
}
//

void DialogImpl::on_pushShow_clicked(bool checked)
{
	// TODO
	QString str;
	QStringList list;
	QString lineNum = "";	
	QString cmd;
	
	str = lineLine->text();
	if (str.length() < 1)
		return;
	
	list = str.split (":");
	str = list.at (0);
	if (list.size() == 2) 
		lineNum = list.at(1);
		
	process = new QProcess;
	cmd = QString ("gvim ") + str;
	if (lineNum.length() > 0)
		cmd += " +" + lineNum;
	process->start (cmd);
	printf ("%s\n", cmd.toStdString().c_str ());
	lineLine->setText ("");	
}

