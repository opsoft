#ifndef DIALOGIMPL_H
#define DIALOGIMPL_H
//
#include <QDialog>
#include <QProcess>
#include "ui_dialog.h"
//
class DialogImpl : public QDialog, public Ui::Dialog
{
Q_OBJECT
public slots:
	void on_pushShow_clicked(bool checked);
public:
	QProcess * process;

	DialogImpl( QWidget * parent = 0, Qt::WFlags f = 0 );
private slots:
};
#endif




