#!/usr/bin/ruby

def usage
	puts "usage: newclass.rb <class name>"
	exit 0
end

def copyright
m = <<EOF
/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 * 
 */
EOF

m
end

def source(cname)
	"#{cname}::#{cname} ()\n{\n}\n\n" +
	"#{cname}::~#{cname} ()\n{\n}\n\n"
end

def header(cname)
	"#ifndef DEFINE_#{cname.upcase}_H\n" +
	"\#define DEFINE_#{cname.upcase}_H\n\n" +
	"class #{cname}\n{\n\tpublic:\n" +
	"\t\t#{cname} ();\n" +
	"\t\t~#{cname} ();\n" +
	"};\n\n" +
	"#endif\n\n" 
end

def writeFile(name, data)
	file = File.open(name, "w")
	file.write data
	file.close
end

usage unless ARGV[0]

cname = ARGV[0]
cr = copyright

hfilename = "/tmp/#{cname}.h"
writeFile(hfilename, copyright + "\n\n" + header(cname))
sfilename = "/tmp/#{cname}.cxx"
writeFile(sfilename, copyright + "\n\n" + source(cname))

puts "#{hfilename} ... created"
puts "#{sfilename} ... created"


