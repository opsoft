/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>

int main (int argc, char ** argv)
{
	Connection * c;
	Connection * m_remote;
	Connection * m_stdin;

	Buf * b;
	Poll * m_pull;
	char m_buf[4096];
	int count;


	if (argc != 3) {
		printf ("usage: connection <IP> <PORT>\n");
		return EXIT_SUCCESS;
	}

	c = new Connection;
	c->init ();
	c->setName ("remote");
	m_remote = c;

	m_stdin = new Connection;
	m_stdin->setSocket (fileno (stdin));
	m_stdin->setName ("local");

	if (c->connect (argv[1], atoi (argv[2])) < 0) {
		perror ("connect");
		return EXIT_FAILURE;
	}

	m_pull = new Poll;
	m_pull->add (c);
	m_pull->add (m_stdin);
	
	while (true) {
		count = m_pull->poll (1000);
		if (count < 0) {
			perror ("poll");
			break;
		}
		if (count == 0)
			continue;

		while ((c = m_pull->scan ()) && c) {
			if (c->ioNRead () == 0)
				return EXIT_SUCCESS;

			memset (m_buf, 0, 4096);
			if (EQ (c->name (), "remote")) {
				b = c->recv ();
				if (b) 
					write (1, b->data (), b->len ());
			} else if (EQ (c->name (), "local")) {
				c->read (m_buf, c->ioNRead ());
				chomp (m_buf);
				strcat (m_buf, "\n");
				m_remote->send (m_buf, strlen (m_buf));
			}
		}

	}

	return EXIT_SUCCESS;
}

