/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */
#include <gclib2.h>
#include <FTP.h>

void usage ()
{
	printf ("usage: ftp <server> <user> <password>\n");
}

int main (int argc, char ** argv)
{
	FTP * f;
	char * server;
	char * user;
	char * pass;

	if (argc != 4) {
		usage ();
		return EXIT_SUCCESS;
	}
	
	server =  argv[1];
	user = argv[2];
	pass = argv[3];

	f = new FTP;
	if (f->connect (server, 21) < 0) {
		perror ("connect");
		return EXIT_FAILURE;
	}

	if (f->login (user, pass) < 00) {
		fprintf (stderr, "login failure\n");
		return EXIT_FAILURE;
	}

	delete f;
	return EXIT_SUCCESS;
}


