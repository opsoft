/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <dns.h>

int main (int argc, char ** argv)
{
	char * str;

	dns_init ();
	dns_async_A ("mail.ru");
	dns_async_A ("yandex.ru");
	dns_async_A ("google.ru");

	sleep (3);
	while (dns_loop ());

	str = dns_async_A ("mail.ru");
	printf ("%s\n", str);
	delete str;

	str = dns_async_A ("yandex.ru");
	printf ("%s\n", str);
	delete str;

	str = dns_async_A ("google.ru");
	printf ("%s\n", str);
	delete str;

	return EXIT_SUCCESS;
}

