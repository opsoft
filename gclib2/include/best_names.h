/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_BEST_NAMES_H
#define DEFINE_BEST_NAMES_H

class DConnection;
class DJobs;
class DHeapSort;
class EList;
class EArray;
class HV;

typedef DConnection Connection;
typedef DJobs Jobs;
typedef DHeapSort HeapSort;
typedef EList List;
typedef EArray Array;
typedef HV Hash;

#endif

