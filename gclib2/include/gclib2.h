/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_GCLIB2_H
#define DEFINE_GCLIB2_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <dirent.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/mman.h>
#include <sys/poll.h>

#ifdef __cplusplus

#include <best_names.h>
#include <Macroses.h>
#include "dlist.h"
#include "elist.h"
#include "darray.h"
#include "earray.h"
#include "Tree.h"
#include "dheapsort.h"
#include "dpbuf.h"
#include <Buf.h>
#include "dconnection.h"
#include "Poll.h"
#include "djobs.h"
#include "dudp.h"
#include "String.h"
#include <hv.h>
#endif

#include "djob_t.h"
#include "dexec.h"
#include <Proto.h>

#endif

