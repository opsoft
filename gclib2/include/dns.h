/*
 * (c) Oleg Puchinin 2006-2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_DNS_H
#define DEFINE_DNS_H

#ifndef DEFINE_GCLIB2_H
#include <gclib2.h>
#endif

#ifndef DEFINE_DNS_INTERNALS_H
struct dns_query;
struct dns_reply;
struct dns_header;
#endif

class DNS
{
	public:
		DNS ();
		~DNS ();

		int init ();
		dns_query * loop () ;
		void set_timeout (int sec) ;
		int get_socket () ;
		void clean () ;
		char * A (char *host) ;
		char * async_A (char *host, dns_query ** qRet = NULL) ; 
		char * MX (char *host) ; 
		char * async_MX (char *IP, dns_query ** qRet = NULL) ;
		char * ip2name (char *IP) ; 
		char * async_ip2name (char *IP, dns_query ** qRet = NULL) ;

	private:
		char * bestServer () ;
		dns_query * query_async (uint16_t flags, uint16_t dr_type, uint16_t dr_class, char * host) ;
		List * query_sync(uint16_t flags, uint16_t dr_type, uint16_t dr_class, char * host,
				char ** d_resp = NULL, int *d_resp_len = NULL) ;
		dns_reply * scan (List *list, uint16_t dr_type, uint16_t dr_class) ;
		__dlist_entry_t * inquiries_scan (char * host, uint16_t dr_type, uint16_t dr_class) ;
		dns_reply * bestMXReply (List * list) ;
		char * __name2dns (char *name, int *len) ;
		char * __ddup (char * ptr, unsigned char ch) ;
		char * __dns_rd_chunk (DPBuf * p, char ** ptr, char * done) ;
		char * __dns_resp_domain (DPBuf * p, char * domain, char **domain_end) ;
		char * make_dns_pkt (uint16_t id, uint16_t flags, uint16_t d_type,
				uint16_t d_class, char * domain, int *len) ;
		List * resp_split (dns_header *hdr, char *pkt, int len) ;
		void resp_clean (List * list) ;
		char * ip2arpa (char *IP) ;
		void clean_query (dns_query * q) ;

		int dns_sock;
		int dns_last_id;
		int dns_timeout;
		List * dns_servers;
		List * dns_inquiries;
};


__export int dns_init () ;
__export char * dns_A (char *host) ;
__export char * dns_async_A (char *host, dns_query ** qRet = NULL) ;
__export char * dns_MX (char *host) ;
__export char * dns_async_MX (char *IP, dns_query ** qRet = NULL) ;
__export char * dns_ip2name (char *IP) ;
__export char * dns_async_ip2name (char *IP, dns_query ** qRet = NULL) ;
__export void dns_clean () ;
__export int dns_get_socket () ;
dns_query * dns_loop () ;

#endif 

