/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_STRINGS_LOW_H
#define DEFINE_STRINGS_LOW_H

char * gc_realloc (char * PTR, int old_size, int new_size) ;
void * memdup (void * PTR, int size) ;
char * Dstrmid (char * lpsz_string,char * param1, char * param2) ;
char * chomp (char * S) ;
char * strchr_r (char * S, char ch, int d_len) ;
char * strchrs (char *S, char ch, char ch2, char ch3, char ch4) ;
char * Dstrstr_r (char *where, char * str) ;
int Dsyms (char * from, char * to, char sym) ;
char * Dmemchr (char * from, int n, char ch) ;
char * Dstrndup (char *ptr, int n) ;
char * Dmid_strchr (char *ptr, char *end, char ch) ;
char * Dmid_getstr (char *buf, char *end) ;
char * Drand_str (char * buf, int count) ;
char * int2str (int i) ;
char * stail (char *S) ;
char * strmov (char *buf, char * S) ;
char * strnmov (char *buf, char * S, int N) ; 
char * strip (char *str) ;
char * strip2 (char *str) ;
char * Dmemmem (char *haystack, size_t haystacklen, char *needle, size_t needlelen) ;
char * Dmid_memmem (char * begin, char * last, char * needle, int needlelen) ;
char * Dsprintf (char * fmt, ...) ;

char * strinsert (char * base, char *ptr, char *ins, int rewrite) ; // v2.2
char * strreplace (char * buf, char * oldstr, char * newstr) ; // v2.2
 
#endif


