/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_GC_IO_H
#define DEFINE_GC_IO_H

/*** ./IO.cxx ***/
int Dfnwrite (char * p_lpsz_filename, void * p_lp_buffer,int int_size) ;
int Dfnread (char * f_name, void * p_lp_buffer, int int_size) ;
int Dselect (int FILENO, int SEC, int USEC) ;
char * DFILE (const char * m_filename, int *rsize) ;
struct stat * DSTAT (const char * S) ;
struct stat * DLSTAT (const char * S) ;
int DIONREAD (int fd) ;
int fsize (const char * S) ;
int fdsize (int fd) ;
char * DFDMAP (int fd) ;
char * DFMAP (const char *d_file, int *out_fd, int *d_out_size) ;
char * Dread_to_eof (int fd, int *d_out_size) ;
char * allData (int fd, int * d_out_size) ;
int move_stream (int fd_in, int fd_out) ;
int move_stream_file (FILE * m_src, FILE * m_dst) ;
int Dnonblock (int fd) ;
int close_pipe (int *fds) ;
int Dtmpfd (char *name) ;
FILE * Dtmpfile (char *name) ;
int fdclose (int * fd) ;
char * fext (char *name) ;
int logToFile (char * fileName, char * fmt, ...) ;
int copyFile (char * sourceName, char * destName) ;
char * DSTR (FILE * m_file) ;
int Dcopyfd (int m_source, int m_dest, int N) ;
int Dcopyfile (FILE * m_source, FILE * m_dest, int N) ;

/*
List * file (char * fileName); // v2.2
List * Dfiles (char * path) ;
*/

#endif

