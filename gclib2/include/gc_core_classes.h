/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_GC_CORE_CLASSES_H
#define DEFINE_GC_CORE_CLASSES_H

#include <Macroses.h>
#include <dpbuf.h>
#include <Buf.h>
#include <darray.h>
#include <earray.h>
typedef EArray Array;
#include <dlist.h>
#include <elist.h>
typedef EList List;
#include <hv.h>
typedef HV Hash;
#include <String.h>
#include <Tree.h>
typedef EArray Array;

#endif

