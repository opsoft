/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include <stdint.h>
#include <stdio.h>
#include <gc_core_classes.h>
#include "internals.h"
#include "dns.h"
#include <Pkt.h>
#include <gc_network.h>
#include <gc_io.h>
#include <gc_strings_low.h>
#include <gc_strings.h>

DNS * m_current_dns = NULL;

DNS::DNS ()
{
	init ();
}

DNS::~DNS ()
{
	clean ();
}

char * DNS::bestServer ()
{
	dns_server * d_server;
	char * Ret;

	d_server = (dns_server *) dns_servers->shift ();
	Ret = d_server->IP;
	dns_servers->push (LPCHAR (d_server));

	return Ret;
}

__export dns_query * DNS::loop ()
{
	int count;
	char * buf = NULL;
	int resp_len;
	dns_header * hdr = NULL;
	dns_query * q = NULL;
	dns_query * Ret = NULL;

dloop_check_again:
	count = DIONREAD (dns_sock);
	if (count <= 0)
		return Ret;

	buf = CNEW (char, count);
	resp_len = recvFrom (dns_sock, buf, count, NULL, 0);
	if (resp_len < (int) sizeof (dns_header)) 
		goto dq_outclean;
	hdr = (dns_header *) buf;
	
	q = (dns_query *) dns_inquiries->first ();
	while (true) {
		q = (dns_query *) dns_inquiries->get ();
		if (! q)
			break;

		if (q->id == htons (hdr->id)) {
			q->reply_pkt = buf;
			q->reply_pkt_len = resp_len;
			break;
		}

		dns_inquiries->next ();
	}
	
	if (q) {
		Ret = q;
		goto dloop_check_again;
	}

dq_outclean:
	DROP (buf);
	return Ret;
	
}

dns_query * DNS::query_async(uint16_t flags, uint16_t dr_type, uint16_t dr_class, char * host)
{
	dns_query * q;
	char * pkt;
	int len;

	if (! host)
		return NULL;
	
	pkt = make_dns_pkt (++dns_last_id, 0x0100, dr_type, dr_class, host, &len);
	if (sendToSocket (dns_sock, pkt, len, bestServer (), 53) < 0) {
		DROP (pkt);
		return NULL;
	}

	q = CNEW (dns_query, 1);
	memset (q, 0, sizeof (dns_query));
	
	q->id = dns_last_id;
	q->flags = flags;
	q->dr_type = dr_type;
	q->dr_class = dr_class;
	q->host = strdup (host);
	dns_inquiries->add (LPCHAR (q));

	return q;
}

List * DNS::query_sync(uint16_t flags, uint16_t dr_type, uint16_t dr_class, 
		char * host, char ** d_resp, int *d_resp_len)
{
	char * pkt;
	int len;
	int count = 0;
	int resp_len;
	char * buf;
	struct dns_header *hdr;
	List * list;

	if (! host)
		return NULL;

	pkt = make_dns_pkt (++dns_last_id, 0x0100, dr_type, dr_class, host, &len);
	if (sendToSocket (dns_sock, pkt, len, bestServer (), 53) < 0) {
		DROP (pkt);
		return NULL;
	}

	if (Dselect (dns_sock, dns_timeout, 0) <= 0) {
		DROP (pkt);
		return NULL;
	}
	
	count = DIONREAD (dns_sock);
	buf = CNEW (char, count);
	resp_len = recvFrom (dns_sock, buf, count, NULL, 0);
	if (resp_len <= 0) 
		return NULL;
	
	hdr = (dns_header *) pkt;
	list = resp_split (hdr, buf, resp_len);
	
	if (d_resp) 
		*d_resp = buf;
	else
		DROP (buf);

	if (d_resp_len)
		*d_resp_len = resp_len;
	
	return list;
}

dns_reply * DNS::scan (List *list, uint16_t dr_type, uint16_t dr_class)
{
	dns_reply *Ret = NULL;
	dns_reply *reply = NULL;

	if (! list)
		return NULL;

	reply = (dns_reply *) list->first ();
	if (! reply)
		return NULL;

	while (true) {
		reply = (dns_reply *) list->get ();
		if (! reply)
			break;
		if ((reply->dr_type == dr_type) && (reply->dr_class == dr_class)) {
			Ret = reply;
			break;
		}
		list->next ();
	}

	return Ret;
}

__dlist_entry_t * DNS::inquiries_scan (char * host, uint16_t dr_type, uint16_t dr_class)
{
	dns_query * q;

	if (! host)
		return NULL;

	dns_inquiries->first ();
	while (true) {
		q = (dns_query *) dns_inquiries->get ();
		if (! q)
			break;
		if (q->dr_type == dr_type && q->dr_class == dr_class &&
				EQ (q->host, host)) 
			return dns_inquiries->get_entry ();
		dns_inquiries->next ();
	}

	return NULL;
}

int DNS::init ()
{
	FILE * f_resolv;
	dns_server * one;
	char m_buf[256];
	int n = 0;
	char *S;

	f_resolv = fopen ("/etc/resolv.conf", "r");
	if (! f_resolv) 
		return -1;

	dns_sock = dUdpSocket ();
	if (dns_sock < 0) {
		fclose (f_resolv);
		return -1;
	}

	dns_inquiries = new List;
	m_buf[255] = 0;
	dns_servers = new List;
	while (fgets (m_buf, 255, f_resolv)) {
		chomp (m_buf);
		if (strncmp (m_buf, "nameserver", 10)) // != nameserver
                        continue;
                
                S = &m_buf[10];
                ++S;
                while (*S && (*S == ' ' || *S == '\t'))
                        ++S;
                
                if (! strlen (m_buf))
                        continue;
                
                one = CNEW (dns_server, 1);
                one->IP = strdup (S);
                one->ms = 0;
                dns_servers->add_tail (LPCHAR (one));
                ++n;		
	}

	fclose (f_resolv);
	if (n == 0) {
		fdclose (&dns_sock);
		return -1;
	}
	
	dns_last_id = rand ();
	return 0;
}

__export void DNS::set_timeout (int sec)
{
	dns_timeout = sec;
}

__export int DNS::get_socket ()
{
	return dns_sock;
}

__export void DNS::clean ()
{
	dns_query * q;

	while (true) {
		q = (dns_query *) dns_inquiries->first ();
		if (! q)
			break;
		dns_inquiries->rm ();
		clean_query (q);
	}
}

__export char * DNS::A (char *host)
{
	struct dns_reply *reply;
	List * list;
	char * Ret = NULL;

	list = query_sync (0x0100, DNS_A, 1, host);
	reply = scan (list, DNS_A, 1);
	if (reply)
		Ret = strdup (inet_ntoa (*((in_addr *) reply->data)));

	resp_clean (list);
	return Ret;
}

__export char * DNS::async_A (char *host, dns_query ** qRet)
{
	__dlist_entry_t * one;
	struct dns_reply *reply;
	dns_query * q;
	List * list;
	char * Ret = NULL;

	one = inquiries_scan (host, DNS_A, 1);
	if (! one) {
		q = query_async (0x0100, DNS_A, 1, host);
		if (qRet)
			*qRet  = q;
		return NULL;
	}

	q = (dns_query *) one->data;
	if (q->reply_pkt == NULL)
		return NULL;

	dns_inquiries->del (one);
	list = resp_split ((dns_header *) q->reply_pkt, q->reply_pkt, q->reply_pkt_len);
	reply = scan (list, DNS_A, 1);
	if (reply)
		Ret = strdup (inet_ntoa (*((in_addr *) reply->data)));

	resp_clean (list);
	DROP (q->host);
	DROP (q->reply_pkt);
	DROP (q);
	return Ret;
}


dns_reply * DNS::bestMXReply (List * list)
{
	struct dns_reply *reply;
	struct dns_reply *best_reply = NULL;
	uint16_t d_min = 0xFFFF;
	char * ptr;
	int i;

	if (! list)
		return NULL;

	if (! list->first ())
		return NULL;

	while (true) {
		reply = (dns_reply *) list->get ();
		if (! reply)
			break;
		
		if (reply->dr_type == 15) {
			ptr = (char *) reply;
			i = htons (pkt_R16(&ptr));
			if (i < d_min) {
				d_min = i;
				best_reply = reply;
			}
		}
		list->next ();
	}
	
	return best_reply;
}

/// Получить (лучшую) MX запись для хоста.
__export char * DNS::MX (char *host)
{
	struct dns_reply *best_reply;
	List * list;
	char * Ret = NULL;
	char * resp;
	int resp_len;
	char * mx;
	DPBuf * p;

	list = query_sync (0x0100, DNS_MX, 1, host, &resp, &resp_len);
	if (! list)
		return NULL;

	best_reply = bestMXReply (list);
	if (! best_reply) {
		resp_clean (list);
		return NULL;
	}

	mx = best_reply->pkt_data_ptr;
	mx += 2;
	p = new DPBuf (resp, resp_len);
	Ret = __dns_resp_domain (p, mx, NULL);

	delete p;
	DROP (resp);
	resp_clean (list);
	return Ret;
}

__export char * DNS::async_MX (char *IP, dns_query ** qRet)
{
	__dlist_entry_t * one;
	struct dns_reply *reply;
	dns_query * q;
	List * list;
	char * Ret = NULL;
       	DPBuf * p;
	char * mx;

	if (! IP)
		return NULL;

	one = inquiries_scan (IP, DNS_MX, 1);
	if (! one) {
		q = query_async (0x0100, DNS_MX, 1, IP);
		if (qRet)
			*qRet  = q;
		return NULL;
	}

	q = (dns_query *) one->data;
	if (q->reply_pkt == NULL)
		return NULL;
	dns_inquiries->del (one);

	list = resp_split ((dns_header *) q->reply_pkt, q->reply_pkt, q->reply_pkt_len);
	reply = bestMXReply (list);

	if (! reply) {
		clean_query (q);
		resp_clean (list);
		return NULL;
	}

	mx = reply->pkt_data_ptr;
	mx += 2;
	p = new DPBuf (q->reply_pkt, q->reply_pkt_len);
	Ret = __dns_resp_domain (p, mx, NULL);
	
	delete p;
	clean_query (q);
	resp_clean (list);
	return Ret;
}

__export char * DNS::ip2name (char *IP)
{
	struct dns_reply *reply;
	char *buf;
	List * list;
	char * Ret = NULL;
	char * resp;
	int resp_len;
	DPBuf * p;
	char * name;
	
	if (! IP)
		return NULL;

	buf = ip2arpa (IP);
	list = query_sync (0x0100, DNS_PTR, 1, buf, &resp, &resp_len);
	DROP (buf);
	reply =scan (list, DNS_PTR, 1);

	if (! reply) 
		goto dip_out;

	name = reply->pkt_data_ptr;
	p = new DPBuf (resp, resp_len);
	Ret = __dns_resp_domain (p, name, NULL);
	delete p;
	DROP (resp);
	
dip_out:
	resp_clean (list);
	return Ret;
}

__export char * DNS::async_ip2name (char *IP, dns_query ** qRet)
{
	__dlist_entry_t * one;
	struct dns_reply *reply;
	dns_query * q;
	List * list;
	char * Ret = NULL;
	char * arpa_name;
       	DPBuf * p;
	char * name;

	arpa_name = ip2arpa (IP);
	if (! arpa_name)
		return NULL;

	one = inquiries_scan (arpa_name, DNS_PTR, 1);
	if (! one) {
		q = query_async (0x0100, DNS_PTR, 1, arpa_name);
		if (qRet)
			*qRet  = q;
		return NULL;
	}

	q = (dns_query *) one->data;
	if (q->reply_pkt == NULL)
		return NULL;
	dns_inquiries->del (one);

	list = resp_split ((dns_header *) q->reply_pkt, q->reply_pkt, q->reply_pkt_len);
	reply = scan (list, DNS_PTR, 1);
	if (! reply) {
		clean_query (q);
		resp_clean (list);
		return NULL;
	}

	name = reply->pkt_data_ptr;
	p = new DPBuf (q->reply_pkt, q->reply_pkt_len);
	Ret = __dns_resp_domain (p, name, NULL);
	delete p;

	clean_query (q);
	resp_clean (list);
	return Ret;
}

/// Упаковать доменное имя.
char * DNS::__name2dns (char *name, int *len)
{
	char * d_buf;
	char * ptr;
	List * list;
	__dlist_entry_t * one;

	list = new List;
	Dsplit (name, (char *) ".", list);
	d_buf = CNEW (char, 70);
	ptr = d_buf;
	
	one = list->get_head ();
	while (one) {
		pkt_W8 (&ptr, strlen (one->data));
		pkt_WS (&ptr, one->data);
		one = one->next;
	}

	pkt_W8 (&ptr, 0);
	delete list;
	if (len)
		*len = ptr - d_buf;

	return d_buf;
}

char * DNS::__ddup (char * ptr, unsigned char ch)
{
	char *S = CNEW (char, ch+2);
	memcpy (S, ptr, ch);
	S[ch] = '.';
	S[ch+1] = '\0';
	return S;
}

char * DNS::__dns_rd_chunk (DPBuf * p, char ** ptr, char * done)
{
	char * ret;
	unsigned char ch;

	if (! p->check (*ptr, 1))
		return NULL;

	ch = pkt_R8 (ptr);
	if (! ch) {
		*done = -1;
		return NULL;
	}
	
	if (ch & 0xc0) {
		ret = NULL;
		--(*ptr);
	} else {
		if (! p->check (*ptr, ch))
			return NULL;
		ret = __ddup (*ptr, ch);
		(*ptr) += ch;
	}			
	
	return ret;
}

char * DNS::__dns_resp_domain (DPBuf * p, char * domain, char **domain_end)
{
	unsigned char ch;
	uint16_t offset;
	char * ptr;
	char buf[128];
	char *S;
	char done = 0;
	bool d_try = true;
	int n = 20;

	if (! p || ! domain)
		return NULL;
	
	buf[0] = 0;
	memset (buf, 0, 128);
	ptr = domain;

drd_try:
	while (true) {
		S = __dns_rd_chunk (p, &ptr, &done);
		if (done || ! S) 
			break;
		
		strcat (buf, S);
		DROP (S);
	}
	
	if ((domain_end != NULL) & d_try)
		*domain_end = ptr;

	if (! done) {
		if (! p->check (ptr, 2))
			return NULL;
		
		if (! --n)
			return NULL;

		ch = pkt_R8 (&ptr);
		offset = (ch &= ~0xc0) << 8;
		offset += pkt_R8 (&ptr);
		d_try = false;
		ptr = &p->data ()[offset];
		goto drd_try;
	}
	
	if (domain_end && ! d_try)
		*domain_end += 2;
	
	chop (buf);
	if (strlen (buf) > 0)
		return strdup (buf);
	else
		return NULL;
}

/// Собрать пакет-запрос DNS.
char * DNS::make_dns_pkt (uint16_t id, uint16_t flags, uint16_t d_type,
		uint16_t d_class, char * domain, int *len)
{
	char * pkt;
	char * ptr;
	char * dns_name;
	int dns_len;
	struct dns_header hdr;

	pkt = CNEW (char, 512);
	ptr = pkt;
	memset (&hdr, 0, sizeof (struct dns_header));
	hdr.id = htons (id);
	hdr.flags = htons (flags);
	hdr.nr = htons (1);
	pkt_Wstruct (&ptr, &hdr);
	
	dns_name = __name2dns (domain, &dns_len);
	memcpy (ptr, dns_name, dns_len);
	DROP (dns_name);
	ptr += dns_len;

	pkt_W16 (&ptr, htons (d_type)); // "A"
	pkt_W16 (&ptr, htons (d_class)); // Internet
	
	if (len)
		*len = ptr-pkt;

	return pkt;
}

/// Разбить ответ на части.
List * DNS::resp_split (dns_header *hdr, char *pkt, int len)
{
	dns_header *query_hdr;
	dns_rheader *res_hdr = NULL;
	dns_reply *reply = NULL;
	char * d_end;
	char * d_name;
	uint16_t dr_type;
	uint16_t dr_class;
	DPBuf * p = NULL;
	char * ptr;
	List * ret;
	
	p = new DPBuf (pkt, len);
	query_hdr = (dns_header *) p->rd (pkt, sizeof (dns_header));
	if (! p->ok)
		goto drs_out;

	if (query_hdr->id != hdr->id)
		goto drs_out;

	ptr = &p->data ()[sizeof (dns_header)];
	d_name = __dns_resp_domain (p, ptr, &d_end);
	if (d_name == NULL)
		goto drs_out;

	DROP (d_name);
	ptr = d_end;
	
	if (! p->check (ptr, 4))
		goto drs_out;

	dr_type = htons (pkt_R16 (&ptr));
	dr_class = htons (pkt_R16 (&ptr));
	
	reply = NULL;
	ret = new List;

	while (true) {
		reply = CNEW (dns_reply, 1);
		memset (reply, 0, sizeof (dns_reply));
		
		reply->domain = __dns_resp_domain (p, ptr, &d_end);
		if (! reply->domain)  
			break;

		ptr = d_end;
		res_hdr = NULL;
		
		if (! p->check (ptr, sizeof (struct dns_rheader))) 
			break;
	
		res_hdr = pkt_Rstruct (&ptr, dns_rheader);
		reply->TTL = htonl (res_hdr->TTL);
		reply->data_len = htons (res_hdr->len);
		reply->dr_type = htons (res_hdr->dr_type);
		reply->dr_class = htons (res_hdr->dr_class);
		if (! p->check (ptr, reply->data_len)) 
			goto drs_out;

		reply->pkt_data_ptr = ptr;
		reply->data = pkt_RD (&ptr, reply->data_len);
		ret->add_tail (LPCHAR (reply));
	}
	
	delete p;
	return ret;
	
drs_out:
	if (reply) {
		DROP (reply->domain);
		DROP (reply);
		DROP (reply->data);
	}
	
	DROP (res_hdr);
	delete p;
	return NULL;

}

void DNS::resp_clean (List * list)
{
	dns_reply * reply;
	
	if (! list)
		return;

	while (true) {
		reply = (dns_reply *) list->first ();
		if (! reply)
			break;
		
		DROP (reply->domain);
		DROP (reply->data);
		DROP (reply);
		list->rm ();
	}

	delete list;
}

char * DNS::ip2arpa (char *IP)
{
	char buf[256];
	unsigned int tmp[4];
	
	sscanf (IP, "%u.%u.%u.%u", &tmp[3], &tmp[2], &tmp[1], &tmp[0]);
	sprintf (buf, "%u.%u.%u.%u.in-addr.arpa", tmp[0], tmp[1], tmp[2], tmp[3]);
	return strdup (buf);
}

void DNS::clean_query (dns_query * q)
{
	if (! q)
		return;
	delete q->host;
	delete q->reply_pkt;
	delete q;
}

__export int dns_init ()
{
	if (! m_current_dns)
		m_current_dns = new DNS;
	return 0;
}

__export char * dns_A (char *host)
{
	dns_init ();
	return m_current_dns->A (host);
}

__export char * dns_async_A (char *host, dns_query ** qRet)
{
	dns_init ();
	return m_current_dns->async_A (host, qRet);
}

__export char * dns_MX (char *host) 
{
	dns_init ();
	return m_current_dns->MX (host);
}

__export char * dns_async_MX (char *IP, dns_query ** qRet)
{
	dns_init ();
	return m_current_dns->async_MX (IP, qRet);
}

__export char * dns_ip2name (char *IP) 
{
	dns_init ();
	return m_current_dns->ip2name (IP);
}

__export char * dns_async_ip2name (char *IP, dns_query ** qRet)
{
	dns_init ();
	return m_current_dns->async_ip2name (IP, qRet);
}

__export void dns_clean ()
{
	if (m_current_dns) {
		delete m_current_dns;
		m_current_dns = NULL;
	}
}

__export int dns_get_socket ()
{
	if (m_current_dns)
		return m_current_dns->get_socket ();
	return -1;
}

dns_query * dns_loop ()
{
	if (m_current_dns) 
		return m_current_dns->loop ();
	return NULL;
}

