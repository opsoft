/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include <sys/stat.h>
#include <sys/un.h>
#include <sys/poll.h>
#include <fcntl.h>
#include <gclib2.h>
#include <netdb.h>

#ifdef __export
#undef __export
#define __export
#endif

/// \file network.c Низкоуровневые сетевые функции.

/*! \page NetworkModule Сетевой модуль.
 * \see network.c dns.cxx DConnection DPoll
 */

/*! \brief Послать UDP в Unix-Socket.
 * \param lp_data - буфер пакета.
 * \param int_size - размер пакета.
 * \param lpsz_path - адрес (путь) сокета.
 */
__export int sendToPath(void * lp_data, int int_size, char * lpsz_path)
{
	int sock;
	int Res;
	struct sockaddr_un name;

	if (! lp_data)
		return -1;
	if (! lpsz_path)
		return -1;

	memset (&name, 0, sizeof (struct sockaddr_un));
	sock = socket(AF_LOCAL,SOCK_DGRAM, 0);
	name.sun_family = AF_LOCAL;
	strncpy (name.sun_path, lpsz_path, 108);
	Res = sendto (sock, lp_data, int_size, 0, (struct sockaddr *) &name, sizeof (struct sockaddr_un));
	close (sock);
	return Res;
}

/*! \brief Послать UDP (IPv4).
 * \param sock - использовать этот сокет.
 * \param lp_data - буфер пакета.
 * \param int_size - размер пакета.
 * \param address - IP адрес ("xxx.xxx.xxx.xxx");
 * \param port - порт (не сетевой формат).
 */
__export int sendToSocket (int sock, void * lp_data, int int_size, const char * address, uint16_t port)
{
	int Res;
	struct sockaddr_in inet_address;

	memset (&inet_address, 0, sizeof (struct sockaddr_in));
	inet_address.sin_family = AF_INET;
	inet_address.sin_port = htons(port);
	inet_aton (address, &inet_address.sin_addr);

	Res = sendto(sock,lp_data,int_size,0,
			(struct sockaddr *) &inet_address, sizeof (struct sockaddr_in));
	return Res;
}

/*! \brief Послать UDP.
 * \param lp_data - буфер пакета.
 * \param int_size - размер пакета.
 * \param address - IP адрес ("xxx.xxx.xxx.xxx");
 * \param port - порт (не сетевой формат).
 */
__export int sendTo (void * lp_data, int int_size, const char * address, uint16_t port)
{
	int sock;
	int Res;
	struct sockaddr_in inet_address;

	if (! lp_data || ! address)
		return -1;
	
	memset (&inet_address, 0, sizeof (struct sockaddr_in));
	inet_address.sin_family = AF_INET;
	inet_address.sin_port = htons(port);
	inet_aton (address, &inet_address.sin_addr);
	sock = socket(AF_INET,SOCK_DGRAM,0);
	Res = sendto(sock,lp_data,int_size,0,
			(struct sockaddr *) &inet_address,sizeof (struct sockaddr_in));
	close (sock);
	return Res;
}

/*! \brief Обертка для bind(2).
 * \param sock - соответственно сокет.
 * \param addr - IP адрес ("xxx.xxx.xxx.xxx");
 * \param port - порт (не сетевой формат).
 */
__export int dBind (int sock, char * addr, int port)
{
	struct sockaddr_in m_addr;
	
	memset (&m_addr, 0, sizeof (struct sockaddr_in));
	inet_aton (addr, &m_addr.sin_addr);
	m_addr.sin_family = AF_INET;
	m_addr.sin_port = htons (port);
	
	return bind (sock, (struct sockaddr *) &m_addr, sizeof (struct sockaddr_in));
}

/// Получить сокет (AF_INET, SOCK_STREAM).
__export int dSocket ()
{
	return socket (AF_INET, SOCK_STREAM, 0);
}

/// Получить сокет (AF_INET, SOCK_DGRAM).
__export int dUdpSocket ()
{
	return socket (AF_INET, SOCK_DGRAM, 0);
}

/*! \brief Подключить сокет.
 * \param sock - соответственно сокет.
 * \param addr - IP адрес ("xxx.xxx.xxx.xxx");
 * \param port - порт (не сетевой формат).
 */
__export int dConnect (int sock, const char * addr, uint16_t port)
{
	struct sockaddr_in m_addr;
	
	if (! addr)
		return -1;

	memset (&m_addr, 0, sizeof (struct sockaddr_in));
	inet_aton (addr, &m_addr.sin_addr);
	m_addr.sin_family = AF_INET;
	m_addr.sin_port = htons (port);
	
	return connect (sock, (struct sockaddr *) &m_addr, sizeof (struct sockaddr_in));
}

/*! \brief Получить адрес хоста по имени.
 * \param hostName - имя сервера.
 * \param address - результирующий адрес.
 */
__export int getHostByName(const char * hostName,struct in_addr * address)
{
	struct hostent * hostent_hostent;
	if((hostName == 0) | (address == 0)) return -1;
	
	hostent_hostent = gethostbyname(hostName);
	if(! hostent_hostent) 
		return -1; 
	
	*address = *((struct in_addr *) &hostent_hostent->h_addr_list[0][0]);
	endhostent();
	return 0;
}

/*! \brief Получить имя хоста по адресу.
 * \param addr - соответственно адрес.
 * \note Функция не реентрантная.
 */
__export char * getNameByHost(struct in_addr * addr)
{
	struct sockaddr_in address;
	struct hostent * hostent_hostent;

	if(! addr) 
		return 0;
	address.sin_addr = *addr;
	hostent_hostent = gethostbyaddr((const char *) &address.sin_addr, 4, PF_INET);
	if(! hostent_hostent) 
		return 0;

	return hostent_hostent->h_name;
}

/*! \brief Получить UDP. 
 * \param sd - соответственно дескриптор.
 * \param pkt - буфер под пакет.
 * \param size - размер буфера под пакет.
 * \param peer_ip - адрес (откуда пришло).
 * \param peer_port - порт (откуда пришло).
 * \note Буфер под peer_ip должен быть не меньше 16 байт.
 * \date 2006 
*/
__export int recvFrom (int sd, char * buf, int size, char * peer_ip, uint16_t * peer_port)
{
	struct sockaddr_in m_addr;
	socklen_t al;
	char * S;
	int len;
	
	if (buf == NULL)
		return -1;
	
	memset (&m_addr, 0, sizeof (struct sockaddr_in));
	al = sizeof (struct sockaddr_in);
	len = recvfrom (sd, buf, size, 0, (struct sockaddr *) &m_addr, &al);
	if (len < 0)
		return len;
	
	if (peer_ip) {
		peer_ip[0] = '\0';
		S = inet_ntoa (m_addr.sin_addr);
		if (! S) 
			return len;
		strcpy (peer_ip, S);
	}		
	
	if (peer_port)
		*peer_port = ntohs (m_addr.sin_port);
			
	return len;
}

/*! \brief Включить BROADCAST на сокет.
 * \param fd - соответственно сокет.
 */
__export int setBroadcast (int fd) 
{
	int one = 1;
	return setsockopt (fd, SOL_SOCKET, SO_BROADCAST, &one, 4);
}

/*! \brief Найти "готовый" pollfd.
 * \param p - указатель на массив pollfd.
 * \param count - размер.
 * \param pos - начать с этой позиции.
 */
__export int pollScan (struct pollfd *p, int count, int pos)
{
	if (! p)
		return -1;

	if (pos >= count)
		return 0;

	for (; pos < count; ++pos) {
		if (p[pos].revents)
			return pos;	
	}			

	return 0;
}

/*! \brief Добавить дескриптор к пулу.
 * \param d_array - инициализированный массив (элементы - pollfd *)
 * \param fd - требуемый дескриптор.
 * \param events - маска (см. poll(2) )
 */
__export int Dpoll_add (EArray * d_array, int fd, short events)
{
	struct pollfd *d;
	if (d_array == NULL)
		return -1;

	d = CNEW (struct pollfd, 1);
	memset (d, 0, sizeof (struct pollfd));
	d->fd = fd;
	d->events = events;
	d->revents = 0;
	d_array->add (LPCHAR (d));
	
	return 0;
}

/*! \brief Собрать пул
 * \param d - массив элементов pollfd *
 * \param p - адрес результирующего пула, готовый для poll (2)
 * \note Необходимо освобождать пул (DROP) после использования.
 */
__export int Dpoll_coallesce (EArray * d, struct pollfd ** p)
{
	int size;
	int i;
	struct pollfd *my;
	
	if (d == NULL || p == NULL)
		return -1;

	size = d->get_size ();
	if (size == 0)
		return 0;
	
	*p = CNEW (pollfd, d->get_size ());
	for (i = 0; i < size; i++) {
		my = (struct pollfd *) d->get (i);
		(*p)[i].fd = my->fd;
		(*p)[i].events = my->events;
		(*p)[i].revents = 0;
	}	

	return size;
}

__export bool isIP (char * S)
{
	int nDots = 0;

	if (! S)
		return false;

	while (*S) {
		if (*S == '.') 
			++nDots;
		else if (! isdigit (*S)) 
			return false;
		++S;
	}
	
	if (nDots != 3)
		return false;

	return true;

}

__export int dReuseAddr (int sock)
{
	int optval=1;
	return setsockopt(sock,SOL_SOCKET,SO_REUSEADDR,&optval,sizeof(optval));
}

