/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>

List * __url_split (char * url)
{
	List * m_ret;
	char * S;
	char * host_ptr;
	char m_buf[512];

	m_ret = new List;
	strcpy (m_buf, url);
	S = strstr (m_buf, "://");
	
	if (! S) {
		S = m_buf;
		m_ret->add (strdup (""));
	} else {
		*S = '\0';
		S += 3;
		m_ret->add (strdup (m_buf));
	}

	host_ptr = S;
	S = strchr (host_ptr, '/');
	if (S) {
		*S = '\0';
		m_ret->add (strdup (host_ptr));
		++S;
		m_ret->add (strdup (S));
	} else {
		m_ret->add (strdup (host_ptr));
		m_ret->add (strdup (""));
	}

	return m_ret;
}

void __url_done (List * m_list)
{
	if (! m_list)
		return;
	m_list->foreach (free);
	delete m_list;
}

char * url_protocol (char * url)
{
	List * m_list;
	char * ret;

	m_list = __url_split (url);
	ret =  strdup ((*m_list)[0]);
	__url_done (m_list);
	return ret;
}

char * url_host (char * url)
{
	List * m_list;
	char * ret;

	m_list = __url_split (url);
	ret =  strdup ((*m_list)[1]);
	__url_done (m_list);
	return ret;

}

char * url_file (char * url)
{
	List * m_list;
	char * ret;

	m_list = __url_split (url);
	ret =  strdup ((*m_list)[2]);
	__url_done (m_list);
	return ret;

}


