/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */

#include <gclib2.h>
#include <Poll.h>
#include <sys/poll.h>

Poll::Poll ()
{
	m_connections = new List;
	pull = NULL;
}

Poll::~Poll ()
{
	delete m_connections;
}

Connection * Poll::__findConnection (Connection * c)
{
	m_connections->first ();
	while (true) {
		if (m_connections->get () == NULL)
			return NULL;
		if (((Connection *) m_connections->get ()) == c)
			return c;
		m_connections->next ();
	}
	return NULL;
}

Connection * Poll::__findName (char * str)
{
	Connection * c;
	m_connections->first ();
	while (true) {
		c = (Connection *) m_connections->get ();
		if (! c)
			return NULL;
		if (EQ (c->name (), str))
			return c;
		m_connections->next ();
	}
	return NULL;
}

Connection * Poll::add (Connection * c)
{
	m_connections->add (LPCHAR (c));
	return c;
}

Connection * Poll::unlink (Connection * c)
{
	if (__findConnection (c))
		m_connections->rm ();
	return c;
}

pollfd * Poll::poll_build (int * nfds)
{
	int count;
	int i = 0;
	Connection * c;

	if (pull)
		DROP (pull);

	count = m_connections->count ();
	pull = CNEW (pollfd, count);
	m_connections->first ();
	while (true) {
		c = (Connection *) m_connections->get ();
		if (! c)
			break;
		
		pull[i].fd = c->socket ();
		pull[i].events = c->pollFlags();
		pull[i].revents = 0;

		m_connections->next ();
		++i;
	}

	if (nfds)
		*nfds = count;

	return pull;
}

Connection * Poll::scan ()
{
	Connection * c;
	int p_pos = 0;
	
	if (! pull)
		return NULL;

	m_connections->first ();
	while (true) {
		c = (Connection *) m_connections->get ();
		if (! c)
			break;
		if (pull[p_pos].revents) {
			c->setRevents (pull[p_pos].revents);
			pull[p_pos].revents = 0;
			return c;
		}
		++p_pos;
		m_connections->next ();
	}
	
	return NULL;
}

int Poll::poll (int timeout)
{
	int nfds;
	poll_build (&nfds);
	return ::poll (pull, nfds, timeout);
}

int Poll::count ()
{
	return m_connections->count ();
}

List * Poll::connections ()
{
	List * l;
	l = new List;
	*l = *m_connections;
	return l;
}
