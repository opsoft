/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_FTP_H
#define DEFINE_FTP_H

class FTP : Connection
{
	public:
		FTP ();
		FTP (bool passive);
		~FTP ();

		int connect (char * server, uint16_t port = 21);
		int login (char * l, char * p);

	private:
		List * waitReply ();
		int checkReply (int needle);
		int typeI ();
		int pasv ();
		int port ();
		char * syst ();
		bool m_passive;
		Connection * m_dataConnection;
};

#endif

