/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 * 
 */

#define DEFINE_MRIM_HDR

#include <gclib2.h>
#include <gc_network.h>
#include "mmp_proto.h"
#include "MMP.h"
#include <time.h>

#define CMDS_OUT_SIZE 1024
// #define DBG(arg) do { printf (arg); fflush (stdout); } while (0)
#define DBG(arg) 

MMP::MMP ()
{
	srand (time (NULL));
	fd = -1;
	cmds_out = NULL;
	last_seq = 0;
	m_ping = 0;
	m_timeout = rand ();
}

MMP::~MMP ()
{
	logout ();
}

mrim_hdr * 
MMP::mk_pkt (u_long msg, char * attach, uint8_t len)
{
	mrim_hdr *pkt;

	pkt = (mrim_hdr *) CNEW (char, sizeof (mrim_packet_header_t) + len);
	
	bzero (pkt, sizeof (mrim_packet_header_t));
	pkt->magic = CS_MAGIC;
	pkt->proto = PROTO_VERSION;
	pkt->msg = msg;
	pkt->seq = ++last_seq;
	pkt->dlen = len;
	pkt->fromport = (u_long) caddr.sin_port;	
	pkt->from = (u_long) caddr.sin_addr.s_addr;
	if (attach) 
		memcpy (& ((char *) pkt)[sizeof (mrim_packet_header_t)], attach, len);
	
	return pkt;
}

int MMP::hello ()
{
	mrim_hdr * pkt = mk_pkt (MRIM_CS_HELLO);
	if (pkt == NULL)
		return -1;
	return send (fd, (char *) pkt, sizeof (mrim_hdr), 0);
}

char * MMP::rcv (int * len) 
{
	int count;
	char * Ret = NULL;
	
	count = DIONREAD (fd);
	if (count <= 0)
		return NULL;

	Ret = CNEW (char, count);
	if (recv (fd, Ret, count, 0) != count) {
		DROP (Ret);
		return NULL;
	}
	
	if (len)
		*len = count;

	return Ret;		
}

int MMP::real_connect (char *addr, int len)
{
	char * S;
	int port;
	socklen_t slen;

	addr[len-1] = 0;
	
	S = strchr (addr, ':');
	if (S == NULL)
		return -1;

	*S = '\0';
	S++;
	if (*S == '\0')
		return -1;

	port = atoi (S);	
	
	fd = dSocket ();
	if (fd < 0)
		return -1;
	
	if (dConnect (fd, addr, port) < 0) {
		close (fd);
		return -1;
	}
	
	slen = sizeof (struct sockaddr_in);
	getpeername (fd, (struct sockaddr *) &saddr, &slen);
	slen = sizeof (struct sockaddr_in);
	getsockname (fd, (struct sockaddr *) &caddr, &slen);
	
	return 0;
}

int MMP::init () 
{
	struct sockaddr_in addr;
	char * response;
	int len;
	char * ptr;
	mrim_hdr *pkt;
	int * i;
	
	cmds_out = CNEW (char *, CMDS_OUT_SIZE);
	if (cmds_out == NULL)
		return -1;
	
	memset (cmds_out, 0, sizeof (char *) * CMDS_OUT_SIZE);
	
	fd = dSocket ();
	if (fd < 0)
		return -1;
	
	memset (&addr, 0, sizeof (sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = htons (2042);
	if (getHostByName ("mrim.mail.ru", &addr.sin_addr)) {
		fprintf (stderr, "Can't connect to %s\n", "mrim.mail.ru");
		return -1;
	}
	
	DBG ("Connect to main server...\n");
	if (connect (fd, (struct sockaddr *) &addr, sizeof (struct sockaddr_in)) < 0) {
		perror ("connect");
		return -1;
	}
	
	if (Dselect (fd, m_timeout, 0) <= 0) 
		return -1;
	
	response = rcv (&len);
	close (fd);
	
	if (response == NULL) 
		return -1;
	
	DBG ("Connect to real server...\n");
	if (real_connect (response, len) < 0)
		return -1;
	
	DBG ("Send hello...\n");
	DROP (response);
	if (hello () < 0)
		return -1;
		
	if (Dselect (fd, m_timeout, 0) <= 0) 
		return -1;
	
	response = rcv (&len);
	if (response == NULL)  
		return -1;
	
	pkt = (mrim_hdr *) response;
	ptr  = response + sizeof (mrim_hdr);
	if (pkt->msg != MRIM_CS_HELLO_ACK) {
		DROP (response);
		return -1;
	}

	i = (int *) ptr;	
	m_ping = *i;
	return 0;
}

void MMP::LPS (char ** pkt, char *str)
{
	char *S;
	int *i;

	i = (int *) *pkt;
	*i = strlen (str);
	S = *pkt;
	S+=4;
	strcpy (S, str);
	S += *i;
	*pkt = S;
}

void MMP::UL (char ** pkt, int N)
{
	int *i;

	i = (int *) *pkt;
	*i = N;
	*pkt += 4;
}

int MMP::login (char * login, char * pass)
{
	mrim_hdr * pkt;
	char * attach;
	char *ptr;
	int len;
	char * response;
	
	attach = CNEW (char, 4096);
	ptr = attach;
	LPS (&ptr, login);
	LPS (&ptr, pass);
	UL (&ptr, STATUS_ONLINE);
	LPS (&ptr, (char *) "Linux MMP client. (c) Oleg Puchinin graycardinalster@gmail.com");
	pkt = mk_pkt (MRIM_CS_LOGIN2, attach, ptr-attach);
	len = sizeof (mrim_hdr) + (ptr - attach);
	
	DROP (attach)
	if (send (fd, (char *) pkt, len, 0) < 0) 
		return -1;

	DROP (pkt);
	if (Dselect (fd, m_timeout, 0) <= 0) 
		return -1;

	response = rcv (&len);
	if (response == NULL)  
		return -1;

	pkt = (mrim_hdr *) response;
	if (pkt->msg != MRIM_CS_LOGIN_ACK) {
		DROP (response);
		return -1;
	}

	return 0;
}

int MMP::sendMessage (char * to, char * message, bool multicast)
{
	mrim_hdr * pkt;
	char * attach;
	char *ptr;
	int len;
	char * response;
	int * i;
	
	attach = CNEW (char, 4096);
	ptr = attach;

	if (multicast) 
		UL (&ptr, MESSAGE_FLAG_MULTICAST);
	else
		UL (&ptr, 0);
	LPS (&ptr, to);
	LPS (&ptr, message);
	LPS (&ptr, (char *) " ");

	pkt = mk_pkt (MRIM_CS_MESSAGE, attach, ptr-attach);
	len = sizeof (mrim_hdr) + (ptr - attach);
	DROP (attach);

	if (send (fd, (char *) pkt, len, 0) < 0) {
		DROP (pkt);
		return -1;
	}
	DROP (pkt);

	if (Dselect (fd, m_timeout, 0) <= 0) 
		return -1;

	response = rcv (&len);
	pkt = (mrim_hdr *) response;
	if (pkt->msg != MRIM_CS_MESSAGE_STATUS) {
		DROP (response);
		return -1;
	}
	
	ptr = response + sizeof (mrim_hdr);
	i = (int *) ptr;
	if (*i != 0) {
		DROP (response);
		return -1;
	}

	DROP (response);
	return 0;
}

int MMP::pingInterval ()
{
	return m_ping;
}

int MMP::ping ()
{
	int Ret;
	mrim_hdr * pkt;
	pkt = mk_pkt (MRIM_CS_PING, NULL, 0);
	Ret = send (fd, (char *) pkt, sizeof (mrim_hdr), 0);	
	DROP (pkt);
	return Ret;
}

int MMP::socket ()
{
	return fd;
}

int MMP::timeout ()
{
	return m_timeout;
}

int MMP::setTimeOut (int n)
{
	m_timeout = n;
	return n;
}

int MMP::logout ()
{
	fdclose (&fd);
	return 0;
}

