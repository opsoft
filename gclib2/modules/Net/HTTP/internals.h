/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_INTERNALS_H
#define DEFINE_INTERNALS_H

#include "http.h"
#include "http_context.h"

int http_connect (http_context * ctx);
int http_send_request (http_context * ctx);
int http_parse_result (http_context * ctx);
int http_chunked_result_join (http_context * ctx);

#endif

