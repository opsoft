/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <gclib2.h>
#include <dirent.h>

/* 24/06/06 */
/// Получить массив со списком файлов из указанной директории.
__export List * Dfiles (char * path)
{
	DIR * m_dir;
	List * Ret;
	struct dirent * m_dirent;

	if (! path) 
		return NULL;
	
	m_dir = opendir (path);
	if (! m_dir)
		return NULL;
	
	Ret = new List;
	while (true) {
		m_dirent = readdir (m_dir);
		if (! m_dirent)
			break;
		m_dirent = (struct dirent *) memdup (m_dirent, sizeof (struct dirent));
		Ret->add (LPCHAR (strdup (m_dirent->d_name)));
		DROP (m_dirent);
	}

	closedir (m_dir);
	return Ret;
}

