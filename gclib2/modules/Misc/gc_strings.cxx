/*
 * (c) Oleg Puchinin 2006,2007,2008
 * graycardinalster@gmail.com
 *
 */

#include <gc_core_classes.h>
#include <gc_strings.h>

char * strreplace (char * buf, char * oldstr, char * newstr)
{
	List * m_list;
	char * m_buf;

	if (!buf || !oldstr || !newstr)
		return NULL;		

	m_list = split (buf, oldstr);
	if (! m_list)
		return NULL;
	m_buf = join (m_list, newstr);
	m_list->foreach (free);
	
	delete m_list;
	return m_buf;
}


