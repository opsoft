/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_STRINGS_H
#define DEFINE_STRINGS_H

#include <gclib2.h>
#include <gc_strings_low.h>

__export List * split (char * buf, char * spl) ; // v2.2
__export List * bsplit (Buf * m_buf, int c) ; //v2.2
__export List * csplit (Buf * m_buf, Buf * spl, int c = 0) ; // v2.2
__export char * join (List * m_list, char * jn) ; // v2.2
__export Buf * bjoin (List * m_list, Buf * jn); // v2.2

Buf * pack (char * fmt, ...) ;
int unpack (Buf * m_buf, char * fmt, ...) ; // v2.2

#endif

