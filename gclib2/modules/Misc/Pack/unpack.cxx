/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gc_core_classes.h>
#include <stdarg.h>
#include <Pkt.h>
#include "internals.h" 

char * __unpack_simple (char * ptr, char ch, char * param)
{
	char * aparam = (char *) param;
	uint16_t * bparam = (uint16_t *) param;
	int * cparam = (int *) param;
	uint64_t * dparam = (uint64_t *) param;

	switch (ch) {
		case 'a':
			*aparam = pkt_R8 (&ptr);
			break;
		case 'b':
			*bparam = pkt_R16 (&ptr);
			break;
		case 'c':
			*cparam = pkt_R32 (&ptr);
			break;
		case 'd':				
			memcpy ((char *) dparam, (char *) ptr, sizeof (uint64_t));
			ptr += sizeof (uint64_t);
			break;
	}

	return ptr;
}

int __unpack (Buf * buf, char * fmt, va_list ap)
{
	char * ptr;
	char *S;
	char * str;
	int len;
	char ** sparam;
	Buf ** eparam;
	List ** lparam;
	Hash ** hparam;

	if (!buf || !fmt)
		return -1;

	ptr = buf->data ();
	S = fmt;
	while (*S) {
		switch (*S) {
			case 'a':
			case 'b':
			case 'c':
			case 'd':				
				ptr = __unpack_simple (ptr, *S, va_arg (ap, char *));
				break;

			case 'e':		// Buf *
				eparam = va_arg (ap, Buf **);
				len = pkt_R32 (&ptr);
				str = pkt_RD (&ptr, len);
				*eparam = new Buf (str, len);
				break;

			case 's':
				sparam = va_arg (ap, char **); // LPSZ
				len = strlen (ptr);
				*sparam = strdup (ptr);
				ptr += len + 1;
				break;

			case 'l':
				lparam = va_arg (ap, List **); // List
				ptr = unpack_unpacklist (ptr, lparam);
				break;

			case 'h':
				hparam = va_arg (ap, Hash **); // HV
				ptr = unpack_unpackhash (ptr, hparam);
				break;
		}
		++S;
	}

	return 0;
}

int unpack (Buf * m_buf, char * fmt, ...)
{
	va_list ap;
	int Ret;

	va_start (ap, fmt);
	Ret = __unpack (m_buf, fmt, ap);
	va_end (ap);

	return Ret;
}

