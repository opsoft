/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gc_core_classes.h>
#include <stdarg.h>
#include <Pkt.h>
#include "internals.h" 

int pack_countBList (List * m_list)
{
	Buf * b;
	int i = 0;

	if (! m_list)
		return 0;

	m_list->first ();
	while (true) {
		b = (Buf *) m_list->get ();
		if (! b)
			break;
		i += b->len ();
		m_list->next ();
	}

	return  i;
}

