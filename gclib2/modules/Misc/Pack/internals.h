/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_INTERNALS_H
#define DEFINE_INTERNALS_H

int pack_countList (List * m_list) ;
int pack_countBList (List * m_list) ;
int pack_countHash (Hash * m_hash) ;
int pack_countTreeNode (node_t * m_node) ;
int pack_countTree (Tree * m_tree) ;
int pack_countBuf (char * fmt, va_list ap) ;

char * unpack_unpacklist (char * ptr, List ** m_ret) ;
char * unpack_unpackhash (char * ptr, Hash ** m_hash) ;

#endif

