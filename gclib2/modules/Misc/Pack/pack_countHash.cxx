/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gc_core_classes.h>
#include <stdarg.h>
#include <Pkt.h>
#include "internals.h" 

int pack_countHash (Hash * m_hash)
{
	List * m_list;
	int len = 0;

	if (! m_hash)
		return 0;

	m_list = m_hash->keys ();
	len += pack_countList (m_list);
	delete m_list;

	m_list = m_hash->values ();
	len += pack_countList (m_list);
	delete m_list;

	return len;
}

