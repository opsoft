/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gc_core_classes.h>
#include <stdarg.h>
#include <Pkt.h>
#include "internals.h" 

int pack_countTreeNode (node_t * m_node)
{
	int len;
	node_t * n;

	if (! m_node)
		return 0;

	len = 9;
	if (m_node->key) 
		len += strlen (m_node->key);
	
	if (! m_node->childNodes)
		return len;

	m_node->childNodes->first ();
	while (true) {
		n = (node_t *) m_node->childNodes->get ();
		if (! n)
			break;
		len += pack_countTreeNode (n);
		m_node->childNodes->next ();
	}

	return len;
}

int pack_countTree (Tree * m_tree)
{
	node_t * n;
	int len;

	if (! m_tree)
		return 0;
	n = m_tree->rootNode;
	len = pack_countTreeNode (n);

	return len;
}


