/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gc_core_classes.h>
#include <stdarg.h>
#include <Pkt.h>
#include "internals.h" 

char * unpack_unpacklist (char * ptr, List ** m_ret)
{
	int count;
	int len;
	int i;

	if (!ptr || !m_ret)
		return NULL;

	*m_ret = new List;
	count = pkt_R32 (&ptr);
	
	for (i = 0; i < count; ++i) {
		len = strlen (ptr);
		(*m_ret)->add(strdup (ptr));
		ptr += len + 1;
	}

	return ptr;
}

char * unpack_unpackhash (char * ptr, Hash ** m_hash)
{
	List * m_keys;
	List * m_values;
	char * key;
	char * value;

	if (!ptr || !m_hash)
		return NULL;

	ptr = unpack_unpacklist (ptr, &m_keys);
	ptr = unpack_unpacklist (ptr, &m_values);

	*m_hash = new Hash;
	m_keys->first ();
	m_values->first ();
	while (true) {
		key = m_keys->get ();
		value = m_values->get ();
		if (!key || !value) 
			break;
		(*m_hash)->set (key, strdup (value));
		m_keys->next ();
		m_values->next ();
	}

	m_keys->foreach (free);
	m_values->foreach (free);
	delete m_keys;
	delete m_values;

	return ptr;
}

