/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

/// \file Pkt.c Низкоуровневые функции сборки пакетов данных.

/// Записать байт в пакет.
void pkt_W8 (char ** pkt, unsigned char data)
{
	unsigned char *ptr = (unsigned char *) *pkt;
	*ptr = data;
	(*pkt)++;
}

/// Записать слово в пакет.
void pkt_W16 (char ** pkt, uint16_t data)
{
	uint16_t *ptr = (uint16_t *) *pkt;
	*ptr = data;
	(*pkt)+=2;
}

/// Записать двойное слово в пакет. 
void pkt_W32 (char ** pkt, unsigned int data)
{
	unsigned int *ptr = (unsigned int *) *pkt;
	*ptr = data;
	(*pkt)+=4;
}

/// Записать строку в пакет (без нуля).
void pkt_WS (char ** pkt, char *S)
{
	char * ptr = *pkt;
	memcpy (ptr, S, strlen (S));
	(*pkt) += strlen (S);	
}

/// Записать строку в пакет (с нулем).
void pkt_WSZ (char ** pkt, char *S)
{
	char * ptr = *pkt;
	strcpy (ptr, S);
	(*pkt) += strlen (S) + 1;
}

/// Записать данные в пакет.
void pkt_WD (char ** pkt, char *S, int size)
{
	char * ptr = *pkt;
	memcpy (ptr, S, size);
	(*pkt) += size;
}

/// Прочитать байт из пакета.
unsigned char pkt_R8 (char ** pkt)
{
	unsigned char ch = 0;
	ch = *((unsigned char *) *pkt);
	++(*pkt);
	return ch;			
}

/// Прочитать слово из пакета.
uint16_t pkt_R16 (char ** pkt)
{
	uint16_t ret = 0;
	ret = *((uint16_t *) *pkt);
	(*pkt) += 2;
	return ret;
}

/// Прочитать двойное слово из пакета.
uint32_t pkt_R32 (char ** pkt)
{
	uint32_t ret = 0;
	ret = *((uint32_t *) *pkt);
	(*pkt) += 4;
	return ret;
}

/// Прочитать данные (с копированием памяти) из пакета.
char * pkt_RD (char ** pkt, int len)
{
	char * ret = NULL;
	ret = (char *) malloc (len);
	memcpy (ret, *pkt, len);
	(*pkt) += len;
	return ret;
}

