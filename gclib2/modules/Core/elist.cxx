/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gc_core_classes.h>
#include <gc_strings_low.h>
#include <unistd.h>

EList::EList ()
{
        pos = NULL;
}

EList::~EList ()
{
}

char * EList::get ()
{
        if (! pos)
                return NULL;
        else
                return pos->data;
}

char * EList::first ()
{
        pos = get_head ();
        return get ();
}

char * EList::get_first ()
{
	__dlist_entry_t * one = head;
	if (one)
		return one->data;
	return NULL;
}

char * EList::last ()
{
        pos = get_tail ();
        return get ();
}

char * EList::get_last ()
{
	__dlist_entry_t * one = tail;
	if (one)
		return one->data;
	return NULL;
}

char * EList::next ()
{
        if (! pos->next)
                pos = NULL;
        else 
                pos = pos->next;
        return get ();
}

char * EList::prev ()
{
        if (! pos->prev)
                pos = NULL;
        else 
                pos = pos->prev;
        return get ();
}

char * EList::rm ()
{
        __dlist_entry_t *ptr;
        char * Ret;
        
        if (! pos)
                return NULL;

        ptr = pos;
        Ret = ptr->data;
        
        if (ptr->next)
                pos = ptr->next;
        else if (ptr->prev)
                pos = ptr->prev;	
        else 
                pos = NULL;
        
        DList::rm (ptr);
        return Ret;
}

EList & EList::operator << (char * S)
{
	add_tail (S);
	return *this;
}

bool EList::eol () 
{
	return pos ? false : true;
}

char * EList::add_before (char * S)
{
	if (! pos)
		return NULL;
	DList::add_before (pos, S);
	return S;
}

char * EList::add_after (char * S)
{
	if (! pos)
		return NULL;
	DList::add_after (pos, S);
	return S;
}

char * EList::operator [] (int offset)
{
	int i;
	first ();
	for (i = 0; i < offset; ++i)
		next ();
	return get ();
}

char * EList::shift ()
{
	char * S;
	S = first ();
	rm ();
	return S;
}

char * EList::unshift (char * S)
{
	return add_head (S);
}

char * EList::push (char * S)
{
	return add_tail (S);
}

char * EList::pop ()
{
	char *S;
	S = last ();
	rm ();
	return S;
}

void EList::dump ()
{
	char *S;
	first ();
	while (true) {
		S = get ();
		if (! S)
			break;
		printf ("%s\n", S);
		next ();
	}
}

void EList::bdump ()
{
	Buf * b;

	first ();
	while (true) {
		b = (Buf *) get ();
		if (! b)
			break;
		
		if (b->len ())
			write (1, b->data (), b->len ());
		else {
			printf ("<null>");
			fflush (stdout);
		}

		printf ("\n");
		fflush (stdout);
		next ();
	}
}

__dlist_entry_t * EList::get_entry ()
{
	return pos;
}

bool EList::operator == (EList & m_list)
{
	Buf * b1;
	Buf * b2;
	bool Ret = true;;

	first ();
	m_list.first ();
	while (true) {
		b1 = (Buf *) get ();
		b2 = (Buf *) m_list.get ();

		if (!b1 || !b2)
			break;

		if (*b1 != b2) {
			Ret = false;
			break;
		}

		next ();
		m_list.next ();
	}

	if (b1 || b2)
		Ret = false;

	return Ret;
}

bool EList::operator != (List & m_list)
{
	return (!(*this == m_list));
}

Array * EList::toArray ()
{
	Array * a;
	char * ptr;

	a = new Array (count ());
	first ();
	while (! eol ()) {
		ptr = get ();
		a->add (ptr);
		next ();
	}

	return a;
}

List & EList::operator = (List & l)
{
	char * one;

	while (first ())
		rm ();

	for (one = (char *) l.first (); one = (char *) l.get (); l.next ()) 
		add (one);

	return *this;
}

List & EList::operator + (List & l)
{
	char * one;

	for (one = l.first (); one = l.get (); l.next ()) 
		push (one);
	
	return *this;
}

