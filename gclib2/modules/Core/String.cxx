/*
 * (c) Oleg Puchinin 2007,2008
 * graycardinalster@gmail.com
 *
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <gc_strings_low.h>
#include <Macroses.h>
#include <String.h>

String::String ()
{
	m_buf = strdup ("");
}

String::String (const char * str)
{
	m_buf = strdup (str);
}

String::String (char * str)
{
	m_buf = str;
}

String::~String ()
{
	DROP (m_buf);
}

String & String::operator + (char * str)
{
	char * m_new;
	char * S;
	m_new = CNEW (char, strlen (m_buf) + strlen (str) + 1);
	S = strmov (m_new, m_buf);
	strmov (S, str);
	DROP (m_buf);
	m_buf = m_new;
	return *this;
}

String & String::operator + (String & str)
{
	return (*this) + str.c_str ();
}

String & String::operator = (char * str)
{
	if (! str)
		return *this;
	DROP (m_buf);
	m_buf = str;
	return *this;
}

String & String::operator = (const char * str)
{
	if (! str)
		return *this;
	DROP (m_buf);
	m_buf = strdup (str);
	return *this;
}

String & String::operator = (String & str)
{
	DROP (m_buf);
	m_buf = strdup (str.c_str ());
	return *this;
}

String & String::operator << (char * str)
{
	return (*this + str);
}

String & String::operator << (int num)
{
	char m_buf[64];
	sprintf (m_buf, "%i", num);
	return (*this + m_buf);
}

char * String::c_str ()
{
	return m_buf;
}

