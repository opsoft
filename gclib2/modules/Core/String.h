/*
 * (c) Oleg Puchinin 2007,2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_STRING_H
#define DEFINE_STRING_H

class String;
class String
{
	public:
		String ();
		String (char * str);
		String (const char * str);
		~String ();
		
		String & operator + (char * str);
		String & operator + (String & str);
		String & operator = (char * str);
		String & operator = (const char * str);
		String & operator = (String & str);
		
		inline operator char *() {
			return m_buf;
		}
		String & operator << (char * str);
		String & operator << (int num);
		char * c_str ();

	private:
		char * m_buf;
};

#endif


