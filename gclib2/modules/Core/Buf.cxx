/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */

#include <string.h>
#include <Macroses.h>
#include <Buf.h>

void buf_free (void * ptr)
{
	Buf * b;
	b = (Buf *) ptr;
	if (b)
		delete b;
}

Buf::Buf ()
{
}

Buf::Buf (int len)
{
	char * m_buf;
	m_buf = CNEW (char, len);
	init (m_buf, len);
}

Buf::Buf (char * ptr, int len)
{
	init (ptr, len);
}

Buf::~Buf ()
{
	delete begin;
}

char * Buf::seek (int offset, int whence)
{
	switch (whence) {
		case SEEK_SET:
			carret = &begin[offset];
			break;

		case SEEK_CUR:
			carret = &carret[offset];
			break;

		case SEEK_END:
			carret = &end[offset];
			break;
	}

	check (carret, 1);
	return carret;
}

int Buf::available ()
{
	return end - carret;
}

char * Buf::position ()
{
	return carret;
}

Buf * Buf::shift (int count)
{
	Buf * b;
	char * ptr;

	if (count <= 0)
		return NULL;
		
	b = new Buf (count);
	memcpy (b->data (), begin, count);
	carret += count;

	count = available ();
	ptr = CNEW (char, count);
	memcpy (ptr, carret, count);
	
	DROP (begin);
	begin = ptr;
	end = &ptr[count];
	carret = begin;
	
	return b;	
}

int Buf::unshift (Buf * m_buf)
{
	int len1;
	int len2;
	char * ptr;
	char * buf;

	if (! m_buf)
		return len ();

	len1 = len ();
	len2 = m_buf->len ();
	buf = CNEW (char, len1+len2);

	ptr = buf;
	memcpy (ptr,m_buf->data (), len2);
	ptr += len2;
	memcpy (ptr, data (), len1);
	
	DROP (begin);
	begin = buf;
	end = &begin[len1 + len2];
	carret = begin;

	return len1 + len2;
}

char * Buf::readString ()
{
	char * ptr;
	char * Ret = 0;

	ptr = ch (carret, '\0');
	if (! ptr)
		return NULL;
	Ret = strdup (carret);
	set_pos (++ptr);

	return Ret;
}

bool Buf::operator == (Buf * m_buf)
{
	char * ptr;
	char * ptr2;
	int len1;
	int len2;

	if (! m_buf)
		return false;

	ptr = data ();
	ptr2 = m_buf->data ();
	len1 = len ();
	len2 = m_buf->len ();

	if (len1 != len2)
		return false;

	if (! memcmp (ptr, ptr2, len1)) 
		return true;

	return false;
}

bool Buf::operator != (Buf * m_buf)
{
	return !(*this == m_buf);
}


