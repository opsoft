/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */

#include "StdHeaders.h"
#include "Macroses.h"
#include <gclib2.h>


/*! \file base64.c Кодирование/декодирование в/из Base64.
 */

const static char base64ABC[] =
"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
typedef unsigned char uchar_t;

#define S_(a) S[n * 3 + a]
#define S__(a,b) (S[n * 3 + a] & b)
#define S__l(a,b,c) ((S[n * 3 + a] & b) << c)

#define B1 buf[(n<<2)] = base64ABC[ (S_(0) >> 2) ];
#define B2 buf[(n<<2) + 1] = base64ABC [ (S__l(0,0x3,4)) | S_(1) >> 4 ];
#define B3 buf[(n<<2) + 2] = base64ABC [ S__l(1,0x0F,2) | S_(2) >> 6];
#define B4 buf[(n<<2) + 3] = base64ABC [ S__(2,0x3F) ];

/*! \brief Закодировать в Base64.
 * \param S - исходный буфер.
 * \param SIZE - размер исходного буфера.
 */
__export uchar_t * base64_code (unsigned char * S, int SIZE)
{
    uchar_t c, *buf;
    int n;
   
    c = (SIZE == 0) ? strlen ((const char *) S) % 3 : SIZE % 3;
    SIZE = (SIZE == 0) ? strlen ((const char *) S) / 3 : SIZE / 3;
   
    buf = (uchar_t *) malloc (SIZE * 4 + 5);
   
    for (n = 0; n < SIZE; ++n) {
	    B1; B2; B3; B4; 
    }
   
    switch (c) {
    case 1:
        B1; B2;   
        buf[(n<<2) + 2] = '=';
        buf[(n<<2) + 3] = '=';
        ++n;
        break;
    case 2:
        B1; B2; B3;
        buf[(n<<2) + 3] = '=';
        ++n;
        break;
    }
    buf[(n<<2)] = 0;
    return buf;
}

#define BASE64(a) ((unsigned char) (strchr (base64ABC, S[(n<<2) + a]) - base64ABC))

/*! \brief Перекодировать из Base64.
 * \param S - исходный буфер.
 * \param SIZE - размер исходного буфера.
 */
__export uchar_t * base64_decode (unsigned char * S, int SIZE)
{
    unsigned char * ptr;
    int n;
   
    if (SIZE % 4)
        return NULL;
   
    SIZE = SIZE ? SIZE / 4 : strlen ((const char *) S) / 4;
    ptr = (uchar_t *) malloc (SIZE * 4 + 5);
   
    for (n = 0; n < SIZE; n++) {
        ptr[n * 3] = (BASE64(0) << 2) | ( BASE64(1) >> 4);
        if (S[(n<<2) + 2] != '=') {
            ptr[n * 3 + 1] = ((BASE64(1) & 0x0F) << 4) | (BASE64(2) >> 2);
            if (S[(n<<2) + 3] != '=')
                ptr[n * 3 + 2] = ((BASE64(2) & 0x03) << 6) | BASE64(3);
            else
                ptr[n * 3 + 2] = 0;
        } else
            ptr[n * 3 +1] = 0;
    }
   
    ++n;
    ptr [n * 3] = 0;
    return ptr;
}

